import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { ApiService } from '../api-service/api.service';

@Injectable({
  providedIn: 'root'
})
export class ScreenService {
  baseUrlScreen: String = "/api/screen";

  constructor(private _apiService: ApiService) { }

  getScreenList(): Observable<Screen[]> {
    return this._apiService.get(`${this.baseUrlScreen}/list`).pipe(map(data => data))
  }
}
