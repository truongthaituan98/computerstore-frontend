export class Ram {
    ramId: Number;
    memory: Number;
    type: String;
    busSpeed: Number;
}
