import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { Brand } from 'src/app/models/brand/brand.model';
import { ApiService } from '../api-service/api.service';

@Injectable({
  providedIn: 'root'
})
export class BrandService {
  baseUrlBrand: String = "/api/brand";

  constructor(private _apiService: ApiService) { }

  getBrandList(): Observable<Brand[]> {
    return this._apiService.get(`${this.baseUrlBrand}/list`).pipe(map(data => data))
  }
}
