import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ProductInsertComponent } from './pages/admin/product-insert/product-insert.component';
import { ProductUpdateComponent } from './pages/admin/product-update/product-update.component';
import { ForbiddenPageComponent } from './pages/forbidden-page/forbidden-page.component';
import { HomePageComponent } from './pages/home-page/home-page.component';
import { LoginPageComponent } from "./pages/login-page/login-page.component";
import { ProductDetailsComponent } from './pages/product-details/product-details.component';
import { RegisterPageComponent } from "./pages/register-page/register-page.component";
import { AuthGuard } from './_helper/auth.guard';
const baseUrlProduct = "products";
const routes: Routes = [
  {path: 'login', component: LoginPageComponent},
  {path: 'register', component: RegisterPageComponent},
  {path: 'forbidden', component: ForbiddenPageComponent},
  {path: '', component: HomePageComponent, canActivate: [AuthGuard]},
  { path: `${baseUrlProduct}/add`, component: ProductInsertComponent, canActivate: [AuthGuard]},
  { path: `${baseUrlProduct}/update/:id`, component: ProductUpdateComponent },
  { path: `${baseUrlProduct}/details/:id`, component: ProductDetailsComponent },

];
@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
