import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Product } from 'src/app/models/product/product.model';
import { AuthService } from 'src/app/services/auth-service/auth.service';
import swal from 'sweetalert';
@Component({
  selector: 'app-home-page',
  templateUrl: './home-page.component.html',
  styleUrls: ['./home-page.component.css']
})
export class HomePageComponent implements OnInit {
  isLoggedIn: Boolean = false
  roles: string[] = []
  userName = "";
  roleName: string = ""
  isLoadProductList: boolean = false;
  constructor(private _router: Router, private _authService: AuthService) { }
  ngOnInit(): void {
    this._authService.authInfo.subscribe(val => {
      this.isLoggedIn = val.loggedIn;
      this.roles = val.roles;
      this.userName = val.userName;
    });
  }

  isAdmin(): boolean {
    for(let i = 0;i < this.roles.length;i++) {
      if(this.roles[i] == 'ROLE_ADMIN') {
        return true;
      } 
    }
    return false;
  }

  setLoadProductList() {
      this.isLoadProductList = !this.isLoadProductList;
  }

  moveToAddProduct() {
    if(this.isAdmin()) {
      this._router.navigate(["/products/add"]);
    } else {
      swal("Access denied!", "You don't have permission to update this product!", "error");
      return false;
    }
  }
}
