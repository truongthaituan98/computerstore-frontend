export class Battery {
    batteryId: Number;
    batteryInfo: String;
    batteryExpiryDate: Number;
    charger: String;
}
