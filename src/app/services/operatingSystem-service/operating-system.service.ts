import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { OperatingSystem } from 'src/app/models/operating-system/operating-system.model';
import { ApiService } from '../api-service/api.service';

@Injectable({
  providedIn: 'root'
})
export class OperatingSystemService {
  baseUrlOperatingSystem: String = "/api/operating_system";

  constructor(private _apiService: ApiService) { }

  getOperatingSystemList(): Observable<OperatingSystem[]> {
    return this._apiService.get(`${this.baseUrlOperatingSystem}/list`).pipe(map(data => data))
  }
}
