import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { CPU } from 'src/app/models/CPU/cpu.model';
import { ApiService } from '../api-service/api.service';

@Injectable({
  providedIn: 'root'
})
export class CPUService {
  baseUrlCPU: String = "/api/cpu";

  constructor(private _apiService: ApiService) { }

  getCPUList(): Observable<CPU[]> {
    return this._apiService.get(`${this.baseUrlCPU}/list`).pipe(map(data => data))
  }
}
