import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';
import { Product } from 'src/app/models/product/product.model';
import { AuthService } from 'src/app/services/auth-service/auth.service';
import { ProductService } from 'src/app/services/product-service/product.service';
import swal from 'sweetalert';
@Component({
  selector: 'app-product-list',
  templateUrl: './product-list.component.html',
  styleUrls: ['./product-list.component.css']
})
export class ProductListComponent implements OnInit {
  products: Product[]
  isLoggedIn: Boolean = false
  roles: string[] = []
  userName = "";
  showLoadingIndicator: Boolean;
  constructor(private _router: Router, private _productService: ProductService, private _authService: AuthService) { }
  ngOnInit(): void {
    this.refreshProductList();
    this._authService.authInfo.subscribe(val => {
      this.isLoggedIn = val.loggedIn;
      this.roles = val.roles;
      this.userName = val.userName;
    });
  }

  refreshProductList() {
    this._productService.getProductList().subscribe((res) => {
      this.products = res as Product[];
    })
  }

  isAdmin(): boolean {
    for(let i = 0;i < this.roles.length;i++) {
      if(this.roles[i] == 'ROLE_ADMIN') {
        return true;
      } 
    }
    return false;
  }

  updateProductById(productId)
  {
    if(this.isAdmin()){
      return this._router.navigate(["/products/update" + `/${productId}`]);
    } else {
      swal("Access denied!", "You don't have permission to update this product!", "error");
      return false;
    }
  }
  getProductById(productId)
  {
    return this._router.navigate(["/products/details" + `/${productId}`]);
  }

  deleteProductById(id) {
    swal({
      title: "Are you sure?",
      text: "Are you sure that you want to delete this product?",
      icon: "warning",
      buttons: ["Cancel", true],
      dangerMode: true,
    })
    .then((willDelete) => {
      if (willDelete) {
        this.showLoadingIndicator = true;
        this._productService.deleteProductById(id).subscribe(data => {
          this.showLoadingIndicator = false;
        swal("Delete successfully!", "You have successfully deleted this product!", "success");
        this.ngOnInit();
        });
      } else {
        swal("Your product is safe!");
        this.showLoadingIndicator = false;
      }
    });
  }
}
