import { Location } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';
import { Battery } from 'src/app/models/battery/battery.models';
import { Brand } from 'src/app/models/brand/brand.model';
import { CPU } from 'src/app/models/CPU/cpu.model';
import { GraphicsCard } from 'src/app/models/graphics-card/graphics-card.model';
import { HardDisk } from 'src/app/models/hard-disk/hard-disk.model';
import { OperatingSystem } from 'src/app/models/operating-system/operating-system.model';
import { Product } from 'src/app/models/product/product.model';
import { Ram } from 'src/app/models/ram/ram.model';
import { Screen } from 'src/app/models/screen/screen.model';
import { BatteryService } from 'src/app/services/battery-service/battery.service';
import { BrandService } from 'src/app/services/brand-service/brand.service';
import { CPUService } from 'src/app/services/CPU-service/cpu.service';
import { GraphicsCardService } from 'src/app/services/graphicsCard-service/graphics-card.service';
import { HardDiskService } from 'src/app/services/hardDisk-service/hard-disk.service';
import { OperatingSystemService } from 'src/app/services/operatingSystem-service/operating-system.service';
import { ProductService } from 'src/app/services/product-service/product.service';
import { RamService } from 'src/app/services/ram-service/ram.service';
import { ScreenService } from 'src/app/services/screen-service/screen.service';
import swal from 'sweetalert';
@Component({
  selector: 'app-product-insert',
  templateUrl: './product-insert.component.html',
  styleUrls: ['./product-insert.component.css']
})
export class ProductInsertComponent implements OnInit {
  product: Product = new Product();
  submitted = false;
  brands: Brand[];
  batteries: Battery[];
  CPUs: CPU[];
  graphicsCards: GraphicsCard[];
  hardDisks: HardDisk[];
  operatingSystems: OperatingSystem[];
  rams: Ram[];
  screens: Screen[];
  selectedBattery = null
  selectedBrand = null
  selectedCPU = null
  selectedGraphicsCard = null
  selectedHardDisk = null;
  selectedOperatingSystem = null;
  selectedRam = null;
  selectedScreen = null;
  showLoadingIndicator: Boolean;
  constructor(private _brandService: BrandService, private _batteryService: BatteryService, 
    private _cpuService: CPUService, private _graphicsCardService: GraphicsCardService, 
    private _hardDiskService: HardDiskService, private _operatingService: OperatingSystemService,
    private _ramService: RamService, private _screenService: ScreenService, 
    private _productService: ProductService, private _router: Router, private location: Location) { }

  ngOnInit(): void {
    this.getAllBattery();
    this.getAllBrand();
    this.getAllCPU();
    this.getAllGraphisCard();
    this.getAllHardDisk();
    this.getAllOperatingSystem();
    this.getAllRam();
    this.getAllScreen();
    this.resetForm();
   }

   resetForm(form?: NgForm) {
    if (form)
      form.reset();
    this.product = {
      id: null,
      name: "",
      price: null,
      color: "",
      weight: null,
      size: null,
      sound: null,
      resolutionsOfWC: "",
      cpu: CPU,
      hardDisk: HardDisk,
      ram: Ram,
      graphicsCard: GraphicsCard,
      screen: Screen,
      battery: Battery,
      operatingSystem: OperatingSystem,
      brand: Brand,
      summary: "",
      image: "",
      quantity: null
    }
  }

  onSubmit(form: NgForm) {
    form.value.battery = { "batteryId":  form.value.batteryId };
    form.value.ram = { "ramId":  form.value.ramId };
    form.value.cpu = { "cpuId":  form.value.cpuId };
    form.value.hardDisk = { "hardDiskId":  form.value.hardDiskId };
    form.value.graphicsCard = { "graphicsCardId":  form.value.graphicsCardId };
    form.value.screen = { "screenId":  form.value.screenId };
    form.value.operatingSystem = { "operatingSystemId":  form.value.operatingSystemId };
    form.value.brand = { "brandId":  form.value.brandId };
    this.showLoadingIndicator = true;
     this._productService.insertProduct(form.value).subscribe(data => {
      this.showLoadingIndicator = false;
      swal("Create successfully!", "You have successfully created this product!", "success");
      this._router.navigate(['/'])
    }, 
    error => console.log(error));
  }

  getAllBattery() {
    return this._batteryService.getBatteryList().subscribe((res)=> {
      this.batteries = res as Battery[];
      this.selectedBattery = this.batteries[0].batteryId;
    })
  }

  getAllBrand() {
    return this._brandService.getBrandList().subscribe((res)=> {
      this.brands = res as Brand[];
      this.selectedBrand = this.brands[0].brandId;
    })
  }

  getAllCPU() {
    return this._cpuService.getCPUList().subscribe((res)=> {
      this.CPUs = res as CPU[];
      this.selectedCPU = this.CPUs[0].cpuId;
    })
  }

  getAllGraphisCard() {
    return this._graphicsCardService.getGraphicsCardList().subscribe((res)=> {
      this.graphicsCards = res as GraphicsCard[];
      this.selectedGraphicsCard = this.graphicsCards[0].graphicsCardId;
    })
  }

  getAllHardDisk() {
    return this._hardDiskService.getHardDiskList().subscribe((res)=> {
      this.hardDisks = res as HardDisk[];
      this.selectedHardDisk = this.hardDisks[0].hardDiskId;
    })
  }

  getAllOperatingSystem() {
    return this._operatingService.getOperatingSystemList().subscribe((res)=> {
      this.operatingSystems = res as OperatingSystem[];
      this.selectedOperatingSystem = this.operatingSystems[0].operatingSystemId;
    })
  }

  getAllRam() {
    return this._ramService.getRamList().subscribe((res)=> {
      this.rams = res as Ram[];
      this.selectedRam = this.rams[0].ramId;
    })
  }

  getAllScreen() {
    return this._screenService.getScreenList().subscribe((res)=> {
      this.screens = res as unknown as Screen[];
      this.selectedScreen = this.screens[0].screenId;
    })
  }

  cancel(){
    this._router.navigate(['/'])
  }
}
