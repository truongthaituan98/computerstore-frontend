export class GraphicsCard {
    graphicsCardId: Number;
    name: String;
    memory: Number;
    type: String;
}
