import { Component, OnInit } from '@angular/core';
import { AbstractControl, FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { UserService } from 'src/app/services/user-service/user.service';
import Validation from 'src/app/_helper/Validation';
import swal from 'sweetalert';
@Component({
  selector: 'app-register-page',
  templateUrl: './register-page.component.html',
  styleUrls: ['./register-page.component.css']
})
export class RegisterPageComponent implements OnInit {
  showLoadingIndicator: Boolean;
  submitted = false;
  registerForm: FormGroup = new FormGroup({
    userName: new FormControl(null, [Validators.required]),
    fullName: new FormControl(null, [Validators.required]),
    email: new FormControl(null, [Validators.required, Validators.email]),
    gender: new FormControl(null),
    password: new FormControl(null, [Validators.required, Validators.minLength(6), Validators.maxLength(40)]),
    confirmPassword: new FormControl(null, [Validators.required, Validators.minLength(6), Validators.maxLength(40)])
  }, {
    validators: [Validation.match('password', 'confirmPassword')]
  });
  constructor(private _router: Router, private _userService: UserService) { }

  ngOnInit(): void {}
  get f(): {[key: string] : AbstractControl} {
    return this.registerForm.controls;
  }
  moveToLogin() {
    return this._router.navigate(['/login']);
  }
  register(): void {
    const body = this.registerForm.value;
    this.submitted = true;
    body.gender = Boolean(body.gender);
    if (this.registerForm.invalid) {
      return;
    } else {
      if(this.f.userName.errors) {
        this.showLoadingIndicator = false;
      }
      this.showLoadingIndicator = true;
      this._userService.getByUsername(body.userName).subscribe(res => {
        if(res == null) {
          this._userService.register(body).subscribe(res => {
            this.showLoadingIndicator = false;
            swal("Register successfully!", "You have successfully registered!", "success");
            setTimeout(() => {
              this._router.navigate(["/login"])
          }, 1000);
          }, err => console.log("Something was wrong!"));
        } else {
          this.showLoadingIndicator = false;
          swal("Login failed!", "The username already exists!", "warning");
        }
      })
    }

  }
}
