import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { BehaviorSubject, Observable } from 'rxjs';
import { distinctUntilChanged, map } from 'rxjs/operators';
import { AuthInfo } from 'src/app/models/auth-info/auth-info.model';
import { ApiService } from '../api-service/api.service';
import { JwtHelperService } from '@auth0/angular-jwt';
@Injectable({
  providedIn: 'root'
})
export class AuthService {
  private loggedIn = new BehaviorSubject<boolean>(this.isAuthenticated());
  private authInfoSubject = new BehaviorSubject<AuthInfo>({
    loggedIn: this.isAuthenticated(),
    roles: this.getRoles(),
    userName: this.getUsername()
  });
  public authInfo = this.authInfoSubject.pipe(distinctUntilChanged());
  public isLoggedIn = this.loggedIn.asObservable().pipe(distinctUntilChanged());
  helper = new JwtHelperService();
  constructor(private apiService: ApiService, private router: Router) { }

  login(credentials): Observable<any> {
    return this.apiService.post('/api/authenticate', credentials)
      .pipe(map(
        data => {
          if (data.loggedIn == false) {
            return data;
          } else {
            this.finishAuthentication(data);
            return data;
          }
        }
      ));
  }
  finishAuthentication(data): void {
    const token = this.helper.decodeToken(data.token);
    const expiresAt = JSON.stringify((token.exp * 1000));
    // save data to local storage
    localStorage.setItem('user-name', token.sub);
    localStorage.setItem('access-token', data.token);
    localStorage.setItem('expires-at', expiresAt);
    localStorage.setItem('user-role', token.role);
    this.loggedIn.next(true);
    // set current data to observable
    this.authInfoSubject.next({
      loggedIn: true,
      roles: this.getRoles(),
      userName: this.getUsername()
    });
  }

  logout(): Observable<any> {
    localStorage.clear();
    this.authInfoSubject.next({
      loggedIn: false,
      roles: this.getRoles(),
      userName: this.getUsername()
    });
    return this.isLoggedIn;
  }

  isAuthenticated(): boolean {
    const expiresAt = JSON.parse(localStorage.getItem('expires-at'));
    return Date.now() < expiresAt;
  }

  getToken(): string {
    return localStorage.getItem('access-token');
  }

  getRoles(): string[] {
    const roles = localStorage.getItem('user-role');
    let rs = [];
    if (roles) {
      rs = roles.split(',');
    }
    return rs;
  }
  
  getUsername(): string {
    return localStorage.getItem('user-name');
  }
}
