import { Injectable } from '@angular/core';
import { map } from 'rxjs/operators';
import { Observable } from 'rxjs';
import { User } from 'src/app/models/user/user.model';
import { ApiService } from '../api-service/api.service';

@Injectable({
  providedIn: 'root'
})
export class UserService {
  user: User;
  constructor(private _apiService: ApiService) { }
  register(body): Observable<any>{
    return this._apiService.post("/api/account/register",body).pipe(map(data => {return data;}));
  }
  getByUsername(userName: string) {
    return this._apiService.get("/api/account"+`/${userName}`).pipe(map(data => data));
  }
}
 