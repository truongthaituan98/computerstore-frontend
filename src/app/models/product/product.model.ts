import { Battery } from "../battery/battery.models";
import { Brand } from "../brand/brand.model";
import { CPU } from "../CPU/cpu.model";
import { GraphicsCard } from "../graphics-card/graphics-card.model";
import { HardDisk } from "../hard-disk/hard-disk.model";
import { OperatingSystem } from "../operating-system/operating-system.model";
import { Ram } from "../ram/ram.model";

export class Product {
    id: Number;
    name: String;
    price: Number;
    color: String;
    weight: Number;
    size: String;
    sound: String;
    resolutionsOfWC: String;
    cpu: any;
    hardDisk: any;
    ram: any;
    graphicsCard: any;
    screen: any;
    battery: any;
    operatingSystem: any;
    brand: any;
    quantity: Number;
    summary: String;
    image: String;
}
