import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { HardDisk } from 'src/app/models/hard-disk/hard-disk.model';
import { ApiService } from '../api-service/api.service';

@Injectable({
  providedIn: 'root'
})
export class HardDiskService {
  baseUrlHardDisk: String = "/api/hard_disk";

  constructor(private _apiService: ApiService) { }

  getHardDiskList(): Observable<HardDisk[]> {
    return this._apiService.get(`${this.baseUrlHardDisk}/list`).pipe(map(data => data))
  }
}
