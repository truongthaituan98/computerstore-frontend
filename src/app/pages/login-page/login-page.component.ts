import { Component, OnInit } from '@angular/core';
import { AbstractControl, FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { AuthInfo } from 'src/app/models/auth-info/auth-info.model';
import { AuthService } from 'src/app/services/auth-service/auth.service';
import { UserService } from 'src/app/services/user-service/user.service';
import swal from 'sweetalert';
@Component({
  selector: 'app-login-page',
  templateUrl: './login-page.component.html',
  styleUrls: ['./login-page.component.css']
})
export class LoginPageComponent implements OnInit {
  isLoggedIn: Boolean = false
  submitted = false;
  roles: string[] = []
  // the loading indicator
  showLoadingIndicator: Boolean;
  loginForm: FormGroup = new FormGroup({
    userName: new FormControl(null, [Validators.required]),
    password: new FormControl(null, [Validators.required, Validators.minLength(6), Validators.maxLength(40)])
  });
  constructor(private _router: Router, private _authService: AuthService, private _userService: UserService) { }

  ngOnInit(): void { }

  moveToRegisterPage() {
    return this._router.navigate(['/register'])
  }
  roleNames: string
  get f(): { [key: string]: AbstractControl } {
    return this.loginForm.controls;
  }
  login() {
    this.submitted = true
    if (!this.loginForm.valid) {
      return;
    }
    else {
      const credentials = this.loginForm.value;
      if (this.f.userName.errors) {
        this.showLoadingIndicator = false;
      }
      this.showLoadingIndicator = true;
      //call method login from userService
      this._userService.getByUsername(credentials.userName).subscribe(res => {
        if (res != null) {
          this._authService.login(credentials).subscribe((res) => {
            this.showLoadingIndicator = false;
            //call object response
            const response: AuthInfo = res as AuthInfo;
            this.roleNames = String(response.roles);
            if (response.loggedIn == false) {
              swal("Login failed!", "The password is incorrect!", "warning");
            } else if (response.loggedIn == true) {
              swal("Login successfully!", "You have successfully logged in!", "success");
              setTimeout(() => {
                this._router.navigate(["/"]);
              }, 1000);
              localStorage.setItem('userName', response.userName);
              localStorage.setItem('statusLogin', String(response.loggedIn));
              localStorage.setItem("roleName", this.roleNames);
            }
          });
        } else {
          this.showLoadingIndicator = false;
          swal("Login failed!", "The username's not exist!", "warning");
        }
      })
    }
  }
}
