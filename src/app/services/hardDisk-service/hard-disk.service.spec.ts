import { TestBed } from '@angular/core/testing';

import { HardDiskService } from './hard-disk.service';

describe('HardDiskService', () => {
  let service: HardDiskService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(HardDiskService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
