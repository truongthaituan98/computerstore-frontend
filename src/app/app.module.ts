import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppComponent } from './app.component';
import { FooterComponent } from './footer/footer.component';
import { HeaderComponent } from './header/header.component';
import { LoginPageComponent } from './pages/login-page/login-page.component';
import { AppRoutingModule } from './app-routing.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HomePageComponent } from './pages/home-page/home-page.component';
import { RegisterPageComponent } from './pages/register-page/register-page.component';
import {HttpClientModule, HTTP_INTERCEPTORS} from '@angular/common/http';
import { JwtInterceptor } from './services/jwt-service/Jwt.Interceptor';
import { AuthGuard } from './_helper/auth.guard';
import { ForbiddenPageComponent } from './pages/forbidden-page/forbidden-page.component';
import { ProductInsertComponent } from './pages/admin/product-insert/product-insert.component';
import { ProductUpdateComponent } from './pages/admin/product-update/product-update.component';
import { ProductListComponent } from './components/product-list/product-list.component';
import { ProductDetailsComponent } from './pages/product-details/product-details.component';
@NgModule({
  declarations: [
    AppComponent,
    FooterComponent,
    HeaderComponent,
    LoginPageComponent,
    HomePageComponent,
    RegisterPageComponent,
    ForbiddenPageComponent,
    ProductInsertComponent,
    ProductUpdateComponent,
    ProductListComponent,
    ProductDetailsComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule
  ],
  providers: [ 
    {
      provide: HTTP_INTERCEPTORS,
      useClass: JwtInterceptor,
      multi: true
    },
    AuthGuard
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
