import { Location } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { Battery } from 'src/app/models/battery/battery.models';
import { Brand } from 'src/app/models/brand/brand.model';
import { CPU } from 'src/app/models/CPU/cpu.model';
import { GraphicsCard } from 'src/app/models/graphics-card/graphics-card.model';
import { HardDisk } from 'src/app/models/hard-disk/hard-disk.model';
import { OperatingSystem } from 'src/app/models/operating-system/operating-system.model';
import { Product } from 'src/app/models/product/product.model';
import { Ram } from 'src/app/models/ram/ram.model';
import { Screen } from 'src/app/models/screen/screen.model';
import { BatteryService } from 'src/app/services/battery-service/battery.service';
import { BrandService } from 'src/app/services/brand-service/brand.service';
import { CPUService } from 'src/app/services/CPU-service/cpu.service';
import { GraphicsCardService } from 'src/app/services/graphicsCard-service/graphics-card.service';
import { HardDiskService } from 'src/app/services/hardDisk-service/hard-disk.service';
import { OperatingSystemService } from 'src/app/services/operatingSystem-service/operating-system.service';
import { ProductService } from 'src/app/services/product-service/product.service';
import { RamService } from 'src/app/services/ram-service/ram.service';
import { ScreenService } from 'src/app/services/screen-service/screen.service';
import swal from 'sweetalert';
@Component({
  selector: 'app-product-update',
  templateUrl: './product-update.component.html',
  styleUrls: ['./product-update.component.css']
})
export class ProductUpdateComponent implements OnInit {
  product: Product;
  brands: Brand[];
  batteries: Battery[];
  CPUs: CPU[];
  graphicsCards: GraphicsCard[];
  hardDisks: HardDisk[];
  operatingSystems: OperatingSystem[];
  rams: Ram[];
  screens: Screen[];
  selectedBattery = null
  selectedBrand = null
  selectedCPU = null
  selectedGraphicsCard = null
  selectedHardDisk = null;
  selectedOperatingSystem = null;
  selectedRam = null;
  selectedScreen = null;
  id = this._route.snapshot.paramMap.get('id');
  showLoadingIndicator: Boolean;
  constructor(private _brandService: BrandService, private _batteryService: BatteryService, 
    private _cpuService: CPUService, private _graphicsCardService: GraphicsCardService, 
    private _hardDiskService: HardDiskService, private _operatingService: OperatingSystemService,
    private _ramService: RamService, private _screenService: ScreenService, private location: Location,
    private _productService: ProductService, private _router: Router, private _route: ActivatedRoute) { }

  ngOnInit(): void {
    this.getAllBattery();
    this.getAllBrand();
    this.getAllCPU();
    this.getAllGraphisCard();
    this.getAllHardDisk();
    this.getAllOperatingSystem();
    this.getAllRam();
    this.getAllScreen();
    this.resetForm();
    this.getProductById(Number(this.id));
  }

  getProductById(id: number) {
    this._productService.getProductById(id).subscribe(res => {
      this.product = res as Product;
      this.selectedBattery = this.product.battery.batteryId;
      this.selectedBrand = this.product.brand.brandId;
      this.selectedCPU = this.product.cpu.cpuId;
      this.selectedGraphicsCard = this.product.graphicsCard.graphicsCardId;
      this.selectedHardDisk = this.product.hardDisk.hardDiskId;
      this.selectedOperatingSystem = this.product.operatingSystem.operatingSystemId;
      this.selectedRam = this.product.ram.ramId;
      this.selectedScreen = this.product.screen.screenId;
    })
  }

  resetForm(form?: NgForm) {
    if (form)
      form.reset();
    this.product = {
      id: null,
      name: "",
      price: null,
      color: "",
      weight: null,
      size: null,
      sound: null,
      resolutionsOfWC: "",
      cpu: CPU,
      hardDisk: HardDisk,
      ram: Ram,
      graphicsCard: GraphicsCard,
      screen: Screen,
      battery: Battery,
      operatingSystem: OperatingSystem,
      brand: Brand,
      summary: "",
      image: "",
      quantity: null
    }
  }
  onSubmit(form: NgForm) {
    this.showLoadingIndicator = true;
    form.value.id = parseInt(this.id);
    form.value.battery = { "batteryId":  form.value.batteryId };
    form.value.ram = { "ramId":  form.value.ramId };
    form.value.cpu = { "cpuId":  form.value.cpuId };
    form.value.hardDisk = { "hardDiskId":  form.value.hardDiskId };
    form.value.graphicsCard = { "graphicsCardId":  form.value.graphicsCardId };
    form.value.screen = { "screenId":  form.value.screenId };
    form.value.operatingSystem = { "operatingSystemId":  form.value.operatingSystemId };
    form.value.brand = { "brandId":  form.value.brandId };
    swal({
      title: "Are you sure?",
      text: "Are you sure that you want to update this product?",
      icon: "warning",
      buttons: ["Cancel", true],
      dangerMode: true,
    })
    .then((willUpdate) => {
      if (willUpdate) {
        this._productService.updateProduct(form.value).subscribe(data => {
        swal("Update successfully!", "You have successfully updated this product!", "success");
          this._router.navigate(['/'])
        });
      } else {
        swal("Your product is safe!");
        this.showLoadingIndicator = false;
      }
    });
  }
  getAllBattery() {
    return this._batteryService.getBatteryList().subscribe((res)=> {
      this.batteries = res as Battery[];
    })
  }

  getAllBrand() {
    return this._brandService.getBrandList().subscribe((res)=> {
      this.brands = res as Brand[];
    })
  }

  getAllCPU() {
    return this._cpuService.getCPUList().subscribe((res)=> {
      this.CPUs = res as CPU[];
    })
  }

  getAllGraphisCard() {
    return this._graphicsCardService.getGraphicsCardList().subscribe((res)=> {
      this.graphicsCards = res as GraphicsCard[];
    })
  }

  getAllHardDisk() {
    return this._hardDiskService.getHardDiskList().subscribe((res)=> {
      this.hardDisks = res as HardDisk[];
    })
  }

  getAllOperatingSystem() {
    return this._operatingService.getOperatingSystemList().subscribe((res)=> {
      this.operatingSystems = res as OperatingSystem[];
    })
  }

  getAllRam() {
    return this._ramService.getRamList().subscribe((res)=> {
      this.rams = res as Ram[];
    })
  }

  getAllScreen() {
    return this._screenService.getScreenList().subscribe((res)=> {
      this.screens = res as unknown as Screen[];
    })
  }

  cancel(){
    this._router.navigate(['/'])
  }
}
