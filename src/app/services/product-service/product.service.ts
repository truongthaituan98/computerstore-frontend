import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { Product } from 'src/app/models/product/product.model';
import { ApiService } from '../api-service/api.service';

@Injectable({
  providedIn: 'root'
})
export class ProductService {
  baseUrlProduct: String = "/api/product";
  constructor(private _apiService: ApiService) { }

  getProductList(): Observable<any>  {
    return this._apiService.get(`${this.baseUrlProduct}/list`).pipe(map(data => data));
  }

  insertProduct(product: Product): Observable<Object> {
    return this._apiService.post(`${this.baseUrlProduct}/admin/save`, product).pipe(map(data => data));
  }

  getProductById(_id: number){
    return this._apiService.get(`${this.baseUrlProduct}/${_id}`).pipe(map(data => data));
  }

  updateProduct(product: Product){
    return this._apiService.put(`${this.baseUrlProduct}/admin/${product.id}`, product).pipe(map(data => data));
  }

 deleteProductById(productId: number) {
    return this._apiService.delete(`${this.baseUrlProduct}/admin/${productId}`).pipe(map(data => data))
  }
}
