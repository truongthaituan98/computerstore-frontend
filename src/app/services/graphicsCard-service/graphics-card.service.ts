import { Injectable } from '@angular/core';
import { ApiService } from '../api-service/api.service';

@Injectable({
  providedIn: 'root'
})
export class GraphicsCardService {
  baseUrlGraphicsCard: String = "/api/graphics_card";

  constructor(private _apiService: ApiService) { }

  getGraphicsCardList() {
    return this._apiService.get(`${this.baseUrlGraphicsCard}/list`)
  }
}
