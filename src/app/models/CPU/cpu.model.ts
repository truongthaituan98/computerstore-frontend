export class CPU {
    cpuId: Number;
    name: String;
    speed: Number;
    technology: String;
    bufferMemory: String;
    turboBoostSpeed: String;
}
