import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { Ram } from 'src/app/models/ram/ram.model';
import { ApiService } from '../api-service/api.service';

@Injectable({
  providedIn: 'root'
})
export class RamService {
  baseUrlRam: String = "/api/ram";

  constructor(private _apiService: ApiService) { }

  getRamList(): Observable<Ram[]> {
    return this._apiService.get(`${this.baseUrlRam}/list`).pipe(map(data => data))
  }
}
