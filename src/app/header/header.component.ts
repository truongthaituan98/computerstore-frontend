import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from '../services/auth-service/auth.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {
  isLoggedIn: Boolean = false
  roles: string[] = []
  userName = "";
  roleName: string = ""
  constructor(private _router: Router, private _authService: AuthService) { }

  ngOnInit(): void {
    this._authService.authInfo.subscribe(val => {
      this.isLoggedIn = val.loggedIn;
      this.roles = val.roles;
      this.userName = val.userName;
    });
  }
  moveToLogin() {
    return this._router.navigate(['/login']);
  }
  moveToHomePage() {
    return this._router.navigate(['/']);
  }
  moveToRegisterPage() {
    return this._router.navigate(['/register']);
  }
  logout() {
    this._authService.logout().subscribe(res => {
      this._router.navigate(["/login"])
    })
  }
}
