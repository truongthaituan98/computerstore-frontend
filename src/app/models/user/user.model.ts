export class User {
    userId: number;
    userName: string;
    fullName: string;
    email: string;
    roles: any;
}
