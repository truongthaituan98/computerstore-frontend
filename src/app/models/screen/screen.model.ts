export class Screen {
    screenId: Number;
    size: Number;
    resolutionsOfScreen: String;
    technology: String;
    touchPanel: Boolean;
}
