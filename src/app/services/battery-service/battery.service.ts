import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { Battery } from 'src/app/models/battery/battery.models';
import { ApiService } from '../api-service/api.service';

@Injectable({
  providedIn: 'root'
})
export class BatteryService {
  baseUrlBattery: String = "/api/battery";

  constructor(private _apiService: ApiService) { }

  getBatteryList(): Observable<Battery[]> {
    return this._apiService.get(`${this.baseUrlBattery}/list`).pipe(map(data => data))
  }
}
